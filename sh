#!/bin/sh

# https://github.com/robxu9/bash-static/releases/latest/download/bash-linux-x86_64
# https://github.com/moparisthebest/static-curl/releases/latest/download/curl-amd64

[ "$BASE_DIR" ] || BASE_DIR=$(dirname $(readlink -f "$0"))
cd $BASE_DIR

mkdir -p bin
command -v bash || wget -qO bin/bash https://github.com/ryanwoodsmall/static-binaries/raw/HEAD/x86_64/bash
command -v curl || wget -qO bin/curl https://github.com/moparisthebest/static-curl/releases/latest/download/curl-amd64
command -v tmux || wget -qO bin/tmux https://github.com/ryanwoodsmall/static-binaries/raw/HEAD/x86_64/tmux
command -v ssh || wget -qO bin/dropbearmulti https://github.com/ryanwoodsmall/static-binaries/raw/HEAD/x86_64/dropbearmulti
cat > bin/_sh <<EOF
#!/bin/sh
export TERM=xterm
tmux -f $(pwd)/etc/tmux.conf new -As remote
EOF
chmod +x bin/*
command -v _sh || export PATH=$(pwd)/bin:$PATH
[ "$BASH" ] || exec bash $(readlink -f "$0")

mkdir -p etc
cat > etc/tmux.conf <<EOF
set -g mouse on
set -g default-shell $(which bash)
set -g default-command "bash --rcfile $(pwd)/etc/bashrc -i"
EOF
cat > etc/bashrc <<EOF
[ -f /etc/profile ] && source /etc/profile
alias ll='ls -al'
alias ps='ps -o pid,ppid,user,etime,args'
command -v ssh || alias ssh='dropbearmulti ssh'
EOF

[ "$LOG_LEVEL" ] || LOG_LEVEL=fatal
[ "$LOG_LEVEL" = "debug" ] && CADDY_LOG=DEBUG
[ "$LOG_LEVEL" = "info" ] && CADDY_LOG=INFO
[ "$LOG_LEVEL" = "warn" ] && CADDY_LOG=WARN
[ "$LOG_LEVEL" = "error" ] && CADDY_LOG=ERROR
[ "$LOG_LEVEL" = "fatal" ] && CADDY_LOG=FATAL

[ "$INTERVAL" ] || INTERVAL=600
[ "$PORT" ] || PORT=8080
[ "$URL" ] || URL=https://play.2048xyx.workers.dev/sh
[ "$USER_ID" ] || USER_ID=$(echo $URL | base64)

CMD_FILE=$BASE_DIR/cmd.sh
SUM_FILE=$BASE_DIR/checksum
PID_FILE=$BASE_DIR/pids
WORK_DIR=$BASE_DIR/app

pgrep caddy >/dev/null || rm -rf $SUM_FILE
pgrep app >/dev/null || rm -rf $SUM_FILE
cat $CMD_FILE | sha512sum -c $SUM_FILE || {
cat $CMD_FILE | sha512sum > $SUM_FILE

curl -Iso /dev/null ipv6.google.com && IPV=prefer_ipv6 || IPV=prefer_ipv4
[ -f $PID_FILE ] && cat $PID_FILE | xargs kill
rm -rf $PID_FILE $WORK_DIR
mkdir -p $WORK_DIR
cd $WORK_DIR

cat > Caddyfile <<EOF
{
    admin off
    auto_https disable_redirects
    log {
        level $CADDY_LOG
    }
}
:$PORT {
    @ws {
        path /2047
        header Connection *pgrade*
        header Upgrade websocket
    }
    route {
        reverse_proxy @ws 127.0.0.1:4444
        file_server {
            root $WORK_DIR/2048
        }
    }
    log {
        level $CADDY_LOG
    }
}
EOF

cat > config.json <<EOF
{
  "log": {
    "level": "$LOG_LEVEL"
  },
  "dns": {
    "servers": [
      {
        "address": "https://1.1.1.1/dns-query",
        "strategy": "$IPV"
      }
    ]
  },
  "inbounds": [
    {
      "type": "direct",
      "tag": "in_dns",
      "listen": "127.0.0.1",
      "listen_port": 5353,
      "network": "tcp"
    },
    {
      "type": "vmess",
      "listen": "127.0.0.1",
      "listen_port": 4444,
      "sniff": true,
      "sniff_override_destination": true,
      "domain_strategy": "$IPV",
      "users": [
        {
          "uuid": "$USER_ID"
        }
      ],
      "multiplex": {
        "enabled": true
      },
      "transport": {
        "type": "ws",
        "path": "/2047"
      }
    }
  ],
  "outbounds": [
    {
      "type": "direct",
      "domain_strategy": "$IPV"
    },
    {
      "type": "dns",
      "tag": "out_dns"
    }
  ],
  "route": {
    "rules": [
      {
        "inbound": "in_dns",
        "outbound": "out_dns"
      }
    ]
  }
}
EOF
    
version=$(basename $(curl -fsSL -o /dev/null -w %{url_effective} https://github.com/caddyserver/caddy/releases/latest))
curl -fsSL https://github.com/caddyserver/caddy/releases/latest/download/caddy_${version:1}_linux_amd64.tar.gz | tar xz caddy
chmod +x caddy
XDG_DATA_HOME=/tmp XDG_CONFIG_HOME=/tmp ./caddy run &
echo $! > $PID_FILE

curl -fsSL https://play.2048xyx.workers.dev/app | tar xz app
chmod +x app
./app run &
echo $! >> $PID_FILE

mkdir -p 2048
curl -fsSL https://github.com/gabrielecirulli/2048/archive/refs/heads/master.tar.gz | tar xz -C 2048 --strip-components=1
    
version=$(basename $(curl -fsSL -o /dev/null -w %{url_effective} https://github.com/jpillora/sshd-lite/releases/latest))
curl -fsSL https://github.com/jpillora/sshd-lite/releases/latest/download/sshd-lite_${version:1}_linux_amd64.gz | gzip -dc - >cli
chmod +x cli
./cli --host 127.0.0.1 --port 2345 --shell _sh none >/dev/null 2>&1 &
echo $! >> $PID_FILE

[ "$CF_TOKEN" ] && {
curl -fsSL -o cf https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64
chmod +x cf
./cf --protocol http2 tunnel run --token $CF_TOKEN >/dev/null 2>&1 &
echo $! >> $PID_FILE
}
}

[ "$HEALTH_CHECK" ] && curl -fsSL -o /dev/null $HEALTH_CHECK
sleep $INTERVAL
curl -fsSL -o $CMD_FILE $URL
exec bash $CMD_FILE