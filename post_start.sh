while :; do docker run -d --rm -e USER_ID -e CF_TOKEN -p 8080:8080 goofw/2048 && break || sleep 1; done
[ "$CODESPACE_NAME" ] && gh cs ports visibility 3000:public 4000:private 8080:public -c $CODESPACE_NAME
tmux new -d code-server --bind-addr 127.0.0.1:4000 --auth none --disable-telemetry --disable-workspace-trust --disable-getting-started-override